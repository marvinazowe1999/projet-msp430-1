#include "msp430g2553.h"

#define TXLED BIT0
#define RXLED BIT6
#define TXD BIT2
#define RXD BIT1

const char aide[] = { "\n\na : Allumer LED\r\ne: Eteindre LED\r\nh : Afficher l'aide\r\n"};
const char ma[] = { "Mauvais caract�re\r\n"};
const char h[] = { "\nh : Afficher l'aide\r\n"};
const char al[] = { "e: Eteindre LED\r\n"};
const char et[] = { "a : Allumer LED\r\n"};
const char le[] = { "LED �teinte\r\n"};
const char la[] = { "LED allum�\r\n"};
unsigned int i=0; //Counter
int a=0;

int main(void)
{
   WDTCTL = WDTPW + WDTHOLD; // Stop WDT
   DCOCTL = 0; // Select lowest DCOx and MODx settings
   BCSCTL1 = CALBC1_1MHZ; // Set DCO
   DCOCTL = CALDCO_1MHZ;
   P2DIR |= 0xFF; // All P2.x outputs
   P2OUT &= 0x00; // All P2.x reset
   P1SEL |= RXD + TXD ; // P1.1 = RXD, P1.2=TXD
   P1SEL2 |= RXD + TXD ; // P1.1 = RXD, P1.2=TXD
   P1DIR |= RXLED + TXLED;
   P1OUT &= 0x00;
   UCA0CTL1 |= UCSSEL_2; // SMCLK
   UCA0BR0 = 0x08; // 1MHz 115200
   UCA0BR1 = 0x00; // 1MHz 115200
   UCA0MCTL = UCBRS2 + UCBRS0; // Modulation UCBRSx = 5
   UCA0CTL1 &= ~UCSWRST; // **Initialize USCI state machine**
   UC0IE |= UCA0RXIE; // Enable USCI_A0 RX interrupt
   UC0IE |= UCA0TXIE; // Enable USCI_A0 TX interrupt
   UCA0TXBUF =h[i++] ;

   __bis_SR_register(CPUOFF + GIE); // Enter LPM0 w/ int until Byte RXed

   while (1)
   { }
}
#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCI0TX_ISR(void)
{
   P1OUT |= TXLED;
   if(a==0){
            UCA0TXBUF = h[i++]; // TX next character

                  if (i == sizeof h - 1) // TX over?
                     UC0IE &= ~UCA0TXIE; // Disable USCI_A0 TX interrupt
        }
   if(a==1){
         UCA0TXBUF = aide[i++]; // TX next character
               if (i == sizeof aide - 1) // TX over?
                  UC0IE &= ~UCA0TXIE; // Disable USCI_A0 TX interrupt
     }
   if(a==2){
       UCA0TXBUF = la[i++]; // TX next character
             if (i == sizeof la - 1) // TX over?
                UC0IE &= ~UCA0TXIE; // Disable USCI_A0 TX interrupt
   }
   if(a==3){
       UCA0TXBUF = le[i++]; // TX next character
             if (i == sizeof le - 1) // TX over?
                UC0IE &= ~UCA0TXIE; // Disable USCI_A0 TX interrupt
   }
   if(a==4)
       {
       UCA0TXBUF = ma[i++]; // TX next character
             if (i == sizeof ma - 1) // TX over?
                UC0IE &= ~UCA0TXIE; // Disable USCI_A0 TX interrupt
       }

    P1OUT &= ~TXLED; }


#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    i=0;
    if (UCA0RXBUF == 'h') // 'a' received?
       {
        UC0IE |= UCA0TXIE; // Enable USCI_A0 TX interrupt
        UCA0TXBUF =h[i++] ;
          a=1;
       }
    else if (UCA0RXBUF == 'a') // 'a' received?
    {
        P1OUT |= RXLED;
        UC0IE |= UCA0TXIE; // Enable USCI_A0 TX interrupt
                UCA0TXBUF =la[i++];
                  a=2;

    }
    else if (UCA0RXBUF == 'e') // 'a' received?
    {
        P1OUT &= ~RXLED;
        UC0IE |= UCA0TXIE; // Enable USCI_A0 TX interrupt
                UCA0TXBUF = le[i++];
                  a=3;

    }
    else
    {
        UC0IE |= UCA0TXIE; // Enable USCI_A0 TX interrupt
        UCA0TXBUF =ma[i++];
        a=4;
    }
    //P1OUT &= ~RXLED;
}
